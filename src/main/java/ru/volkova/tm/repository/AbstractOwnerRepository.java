package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<E> findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return Optional.ofNullable((entities.stream()
                .filter(entity -> id.equals(entity.getId())
                        && userId.equals(entity.getUserId()))
                .findFirst()
                .orElseThrow(ObjectNotFoundException::new)));
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        entities.clear();
    }

    @Override
    public void removeById(final String userId, final String id) {
        entities.removeIf(entity -> id.equals(entity.getId())
                && userId.equals(entity.getUserId())); }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return null;
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<E> findOneByIndex(final String userId, final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return Optional.ofNullable(entities.stream()
                .filter(entity -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList())
                .get(index));
    }

    @Override
    public Optional<E> findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return Optional.ofNullable(entities.stream()
                .filter(entity -> name.equals(entity.getName())
                        && userId.equals(entity.getUserId()))
                .findFirst()
                .orElseThrow(ObjectNotFoundException::new));
    }
    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        entities.removeIf(entity -> userId.equals(entity.getUserId())
                && entity.equals(entities.get(index)));
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        entities.removeIf(entity -> userId.equals(entity.getUserId())
                && name.equals(entity.getName()));
    }

}
