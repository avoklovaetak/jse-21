package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Optional<Task> add(
            final String userId,
            final String name,
            final String description
    ) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(
            final String userId,
            final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return entities.stream()
                .filter(task -> projectId.equals(task.getProjectId())
                        && userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(
            final String userId,
            final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        entities.removeIf(task -> projectId.equals(task.getProjectId())
                && userId.equals(task.getUserId()));
    }

    @Override
    public void bindTaskByProjectId(
            final String userId,
            final String projectId,
            final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        entities.stream()
               .filter(task -> userId.equals(task.getUserId())
                       && taskId.equals(task.getId()))
               .findFirst()
               .map(task ->
                {
                   task.setProjectId(projectId);
                   return task;
                });
    }

    @Override
    public void unbindTaskByProjectId(
            final String userId,
            final String projectId,
            final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        entities.stream()
                .filter(task -> userId.equals(task.getUserId()) && taskId.equals(task.getId())
                        && projectId.equals(task.getProjectId()))
                .findFirst()
                .map(task ->
                {
                    task.setProjectId(null);
                    return task;
                });
    }

}
