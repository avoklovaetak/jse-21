package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public List<User> findAll() {
        return entities;
    }

    @Override
    public Optional<User> findById(final String id) {
        return entities.stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void removeById(final String id) {
        entities.removeIf(user -> id.equals(user.getId()));
    }

    @Override
    public void remove(final User user) {
        entities.remove(user);
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return entities.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return entities.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst();
    }

    @Override
    public void removeByLogin(final String login) {
        entities.removeIf(user -> login.equals(user.getLogin()));
    }

}
