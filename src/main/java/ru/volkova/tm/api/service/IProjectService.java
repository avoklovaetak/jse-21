package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Project;

import java.util.Optional;

public interface IProjectService extends IOwnerService<Project> {

    Optional<Project> add(String userId, String name, String description);

}
