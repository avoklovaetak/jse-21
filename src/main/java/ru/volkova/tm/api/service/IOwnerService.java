package ru.volkova.tm.api.service;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
    List<E> findAll(String userId, Comparator<E> comparator);

    Optional<E> findOneByIndex(String userId, Integer index);

    Optional<E> findOneByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

    Optional<E> updateOneById(String userId, String id, String name, String description);

    Optional<E> updateOneByIndex(String userId, Integer index, String name, String description);

    Optional<E> startOneById(String userId, String id);

    Optional<E> startOneByName(String userId, String name);

    Optional<E> startOneByIndex(String userId, Integer index);

    Optional<E> finishOneById(String userId, String id);

    Optional<E> finishOneByName(String userId, String name);

    Optional<E> finishOneByIndex(String userId, Integer index);

    Optional<E> changeOneStatusById(String userId, String id, Status status);

    Optional<E> changeOneStatusByName(String userId, String name, Status status);

    Optional<E> changeOneStatusByIndex(String userId, Integer index, Status status);

}
