package ru.volkova.tm.api.repository;

import ru.volkova.tm.entity.Task;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface ITaskRepository extends IOwnerRepository<Task> {

    Optional<Task> add(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    void bindTaskByProjectId(String userId, String projectId, String taskId);

    void unbindTaskByProjectId(String userId, String projectId, String taskId);

}
