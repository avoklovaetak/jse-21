package ru.volkova.tm.api.repository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    Optional<E> findById(String userId, String id);

    void clear(String userId);

    void removeById(String userId, String id);

    List<E> findAll(String userId, Comparator<E> comparator);

    Optional<E> findOneByIndex(String userId, Integer index);

    Optional<E> findOneByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

}
