package ru.volkova.tm.api.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    List<User> findAll();

    Optional<User> findById(String id);

    void clear();

    void removeById(String id);

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    void removeByLogin(String login);

}
