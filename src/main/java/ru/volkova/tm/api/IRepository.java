package ru.volkova.tm.api;

import ru.volkova.tm.entity.AbstractEntity;

import java.util.Optional;

public interface IRepository <E extends AbstractEntity> {

    Optional<E> add(E entity);

    void remove(E entity);

}
