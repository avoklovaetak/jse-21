package ru.volkova.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "clear all projects";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
    }

}
