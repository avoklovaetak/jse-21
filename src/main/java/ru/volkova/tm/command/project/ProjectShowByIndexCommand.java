package ru.volkova.tm.command.project;

import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Override
    public String description() {
        return "show project by index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Project> project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        showProject(project.get());
    }

}
