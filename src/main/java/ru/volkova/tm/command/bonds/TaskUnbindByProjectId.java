package ru.volkova.tm.command.bonds;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskUnbindByProjectId extends AbstractProjectTaskClass {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public String description() {
        return "unbind task out of project";
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectTaskService()
                .unbindTaskByProjectId(userId, projectId, taskId);
    }

}
