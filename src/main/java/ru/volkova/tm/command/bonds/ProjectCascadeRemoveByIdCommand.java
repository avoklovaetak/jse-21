package ru.volkova.tm.command.bonds;

import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

public class ProjectCascadeRemoveByIdCommand extends AbstractProjectTaskClass {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-cascade-remove-by-id";
    }

    @Override
    public String description() {
        return "remove project and all project tasks by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT AND ALL TASKS CASCADE]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectTaskService()
                .removeProjectById(userId, projectId);
    }

}
