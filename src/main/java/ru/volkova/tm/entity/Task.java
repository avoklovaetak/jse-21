package ru.volkova.tm.entity;

import ru.volkova.tm.api.entity.IWBS;

public final class Task extends AbstractOwnerEntity implements IWBS {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return id + ": " + getName() + ": " + projectId
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }
    
}
