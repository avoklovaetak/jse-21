package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.api.service.IOwnerService;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E> implements IOwnerService<E> {

    protected final IOwnerRepository<E> ownerRepository;

    protected AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.ownerRepository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findAll(userId);
    }

    @Override
    public Optional<E> findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        ownerRepository.clear(userId);
    }

    @Override
    public void removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        ownerRepository.removeById(userId, id);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return null;
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    public Optional<E> findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findOneByName(userId, name);
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        ownerRepository.removeOneByName(userId, name);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        ownerRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Optional<E> findOneByIndex(final String userId, final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findOneByIndex(userId, index);
    }

    @Override
    public Optional<E> updateOneByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) {

        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        return entity;
    }

    @Override
    public Optional<E> updateOneById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        return entity;
    }

    @Override
    public Optional<E> startOneById(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @Override
    public Optional<E> startOneByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @Override
    public Optional<E> startOneByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        entity.get().setDateStart(new Date());
        return entity;
    }

    @Override
    public Optional<E> finishOneById(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @Override
    public Optional<E> finishOneByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @Override
    public Optional<E> finishOneByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        entity.get().setDateFinish(new Date());
        return entity;
    }

    @Override
    public Optional<E> changeOneStatusById(final String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<E> entity = findOneByName(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public Optional<E> changeOneStatusByName(final String userId, String name, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public Optional<E> changeOneStatusByIndex(final String userId, Integer index, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        return entity;
    }

}
