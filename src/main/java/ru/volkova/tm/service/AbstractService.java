package ru.volkova.tm.service;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public Optional<E> add(final E entity) {
        repository.add(entity);
        return Optional.ofNullable(entity);
    }

    @Override
    public void remove(final E entity) {
        repository.remove(entity);
    }

}
